import Card from "./comps/Card";
import Gif from "./comps/Gif";
import data from "./data/gif-list.json";
import { useState, useEffect, useCallback } from "react";

//initialize first random number
let previous = undefined;

function App() {
  //init state for the current gif as an object
  const [gif, setGif] = useState(null);

  //get a random number which is unique from the previous number
  const rng = useCallback((randomNum) => {
    const min = 0;
    const max = data.gifs.length - 1;
    if (previous === undefined) {
      randomNum = Math.floor(Math.random() * (max - min + 1));
    } else {
      randomNum = Math.floor(Math.random() * (max - min));
      if (randomNum >= previous) {
        randomNum += 1;
      }
    }
    previous = randomNum;
    return previous;
  }, []);

  //get a random gif object from the json data
  const getGif = useCallback(() => {
    setGif(data.gifs[rng()]);
  }, [rng]);

  //load a gif on the first page load
  useEffect(() => {
    getGif();
  }, [getGif]);

  return (
    <div className="App">
      <Card currentGif={gif} />
      <Gif currentGif={gif} getGif={getGif} />
    </div>
  );
}

export default App;
