//framer motion variants for all pages

//variant controlling gif entry and exit animations
const gifVariant = {
  initial: {
    x: "100vw",
    opacity: 0.5,
  },
  center: {
    opacity: 1,
    x: 0,
    transition: {
      duration: 0.2,
      ease: "anticipate",
    },
  },
  exit: {
    x: "-100vw",
    opacity: 0.5,
    transition: {
      duration: 0.2,
      ease: "anticipate",
    },
  },
};

//variant controlling social media icon animations
const liVariant = {
  hover: {
    rotateZ: [0, 20, -17, 0],
    transition: {
      duration: 0.5,
      ease: "easeInOut",
    },
  },
  tap: {
    scale: 0.8,
  },
};

//variant controlling bouncing ball loading animation
const loaderVariants = {
  animation: {
    y: [0, 40],
    transition: {
      y: {
        repeat: Infinity,
        repeatType: "reverse",
        duration: 0.25,
        ease: "easeInOut",
      },
    },
  },
};

export { gifVariant, liVariant, loaderVariants };
