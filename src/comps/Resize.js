import { useState, useLayoutEffect } from "react";

//function to measure the size of the menu container as it changes size
function useDimensions(ref) {
  const [dimensions, setDimensions] = useState({
    width: 0,
    height: 0,
  });

  useLayoutEffect(() => {
    //set the size on the first render
    if (ref.current) {
      setDimensions({
        width: ref.current.offsetWidth,
        height: ref.current.offsetHeight,
      });
    }
    let measureTimeout = null;
    //set the size on each resize event following a 150ms delay to limit calls
    const resizeListener = () => {
      clearInterval(measureTimeout);
      measureTimeout = setTimeout(
        () =>
          setDimensions({
            width: ref.current.offsetWidth,
            height: ref.current.offsetHeight,
          }),
        150
      );
    };
    // set resize listener
    window.addEventListener("resize", resizeListener);

    return () => {
      // remove resize listener
      window.removeEventListener("resize", resizeListener);
    };
  }, [ref]);

  return dimensions;
}

export default useDimensions;
