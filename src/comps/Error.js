import styles from "../styles/error.module.css";

const Error = ({ getGif, setVidError }) => {
  return (
    <div className={styles.errorContainer}>
      <h2>
        <a href="https://youtu.be/RBGW9J3sx1g">"ha ha, what"</a>
      </h2>
      <p>That gif didn't load for some reason...</p>
      <button
        onClick={() => {
          //clears error, gets new gif
          setVidError(null);
          getGif();
        }}
      >
        try it again
      </button>
    </div>
  );
};

export default Error;
