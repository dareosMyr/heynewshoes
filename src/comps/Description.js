import styles from "../styles/descrip.module.css";
import { motion } from "framer-motion";
import Icon from "@iconify/react";
import patreon from "@iconify/icons-mdi/patreon";
import twitch from "@iconify/icons-mdi/twitch";
import website from "@iconify/icons-mdi/web";
import youtube from "@iconify/icons-mdi/youtube";
import twitter from "@iconify/icons-mdi/twitter";
import { liVariant } from "./Variants";

const Description = () => {
  return (
    <>
      <h2 className={styles.title}>
        <span className={styles.hey}>Hey</span>, new shoes?
      </h2>
      <article className={styles.body}>
        <p>
          Welcome to my tribute site for the one and only <br /> Brian David
          Gilbert
        </p>
        <p>
          If somehow you got here and have not heard of him please check out his
          content:
        </p>
        <ul className={styles.listContainer}>
          <motion.li variants={liVariant} whileHover="hover" whileTap="tap">
            <a href="https://twitter.com/briamgilbert">
              <Icon icon={twitter} />
            </a>
          </motion.li>
          <motion.li variants={liVariant} whileHover="hover" whileTap="tap">
            <a href="http://youtube.com/briandavidgilbert">
              <Icon icon={youtube} />
            </a>
          </motion.li>
          <motion.li
            className={styles.patreon}
            variants={liVariant}
            whileHover="hover"
            whileTap="tap"
          >
            <a href="https://www.patreon.com/briandavidgilbert">
              <Icon icon={patreon} />
            </a>
          </motion.li>
          <motion.li variants={liVariant} whileHover="hover" whileTap="tap">
            <a href="http://twitch.tv/briandavidgilbert">
              <Icon icon={twitch} />
            </a>
          </motion.li>
          <motion.li variants={liVariant} whileHover="hover" whileTap="tap">
            <a href="https://www.briandavidgilbert.com/">
              <Icon icon={website} />
            </a>
          </motion.li>
        </ul>
      </article>
    </>
  );
};

export default Description;
