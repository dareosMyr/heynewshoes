import { useState } from "react";
import styles from "../styles/gif.module.css";
import { motion, AnimatePresence } from "framer-motion";
import { gifVariant } from "./Variants";
import IsLoading from "./IsLoading";
import Error from "./Error";

const Gif = ({ currentGif, getGif }) => {
  const [vidError, setVidError] = useState(null);
  const [vidLoading, setVidLoading] = useState(true);

  return (
    <AnimatePresence exitBeforeEnter>
      {/* test if a gif is selected before displaying video component */}
      {!currentGif ? (
        <IsLoading />
      ) : (
        <motion.div
          key={currentGif.gifUrl}
          variants={gifVariant}
          initial="initial"
          animate="center"
          exit="exit"
          className={styles.vdContainer}
        >
          {/* place errors or loading messages as needed  */}
          {vidLoading && <IsLoading />}
          {vidError && (
            <Error
              current={currentGif}
              getGif={getGif}
              setVidError={setVidError}
            />
          )}
          <video
            className={styles.video}
            src={currentGif.gifUrl}
            type="video/webm"
            autoPlay
            loop
            playsInline
            muted
            onLoadedData={() => {
              setVidLoading(false);
            }}
            onLoadStart={() => {
              setVidLoading(true);
            }}
            onError={(e) => {
              setVidError(e.target);
            }}
            onClick={() => getGif()}
          ></video>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default Gif;
