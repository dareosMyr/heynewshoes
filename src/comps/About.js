import styles from "../styles/about.module.css";
import descripStyles from "../styles/descrip.module.css";
import { motion } from "framer-motion";

const About = ({ currentGif }) => {
  return (
    <div>
      <h2 className={descripStyles.title}>About</h2>
      <article>
        <p className={descripStyles.body}>
          Left click or tap the screen to load a new gif <br></br>If you like
          the current gif, use the buttons below <br></br> to see its source:
        </p>
      </article>
      <motion.a
        className={styles.button}
        whileTap={{ scale: 0.8 }}
        href={currentGif.hostLink}
      >
        Gfycat/Tenor
      </motion.a>
      {currentGif.sourceUrl && (
        <motion.a
          whileTap={{ scale: 0.8 }}
          className={styles.button}
          href={currentGif.sourceUrl}
        >
          Original
        </motion.a>
      )}
      <p className={styles.mark}>
        Site designed by <a href="https://www.dareosmeyer.com">dareosMyr</a>
      </p>
    </div>
  );
};

export default About;
