import styles from "../styles/card.module.css";
import { MenuToggle } from "./MenuToggle";
import { motion, useCycle } from "framer-motion";
import { useState, useRef } from "react";
import { Icon } from "@iconify/react";
import helpIcon from "@iconify/icons-mdi/help";
import arrowIcon from "@iconify/icons-mdi/arrow-left";
import Description from "./Description";
import About from "./About";
import useDimensions from "./Resize";

const Card = ({ currentGif }) => {
  const [isOpen, toggleOpen] = useCycle(true, false);
  const [content, toggleContent] = useState(true);
  //toggles back and forth between icons on click
  const [aboutPageIcon, toggleAboutPageIcon] = useCycle(helpIcon, arrowIcon);
  //init the ref to measure the size of the menu container
  const containerRef = useRef();
  let dimensions = useDimensions(containerRef);

  //did not want to use routes in this simple app so made each "page" a toggle component
  const showHelp = () => {
    if (content) {
      toggleContent(false);
      toggleAboutPageIcon();
    } else {
      toggleContent(true);
      toggleAboutPageIcon();
    }
  };

  const returnToDes = () => {
    if (!content) {
      setTimeout(() => {
        toggleContent(true);
        toggleAboutPageIcon();
      }, 500);
    }
  };

  //controls the drawing of the revealing menu circle which must grow and shrink as the screen changes size
  const menuVariant = {
    open: {
      clipPath: `circle(${
        dimensions.height + dimensions.width * 0.8
      }px at 29px 29px)`,
      transition: {
        type: "spring",
        stiffness: 40,
      },
    },
    closed: {
      clipPath: `circle(25px at 30px 28px)`,
      transition: {
        delay: 0.2,
        type: "spring",
        stiffness: 400,
        damping: 40,
      },
    },
  };

  return (
    <main className={styles.container}>
      <motion.div
        initial={false}
        variants={menuVariant}
        animate={isOpen ? "open" : "closed"}
        className={styles.card}
        ref={containerRef}
      >
        <motion.div
          className={styles.helpToggle}
          whileHover={{ scale: 1.1 }}
          whileTap={{ scale: 0.8 }}
          onClick={() => showHelp()}
        >
          <Icon icon={aboutPageIcon} />
        </motion.div>
        <MenuToggle
          style={styles}
          toggle={() => {
            //switches back to the description page after closing the menu
            toggleOpen();
            returnToDes();
          }}
        />
        {content ? <Description /> : <About currentGif={currentGif} />}
      </motion.div>
    </main>
  );
};

export default Card;
