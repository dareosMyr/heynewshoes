import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { loaderVariants } from "./Variants";
import styles from "../styles/loader.module.css";

const IsLoading = () => {
  const [wait, setWait] = useState(false);

  //start a timer to wait a few seconds before telling users to refresh the page
  useEffect(() => {
    const waitTimer = setTimeout(() => {
      setWait(true);
    }, 4000);
    return () => {
      clearInterval(waitTimer);
    };
  }, []);

  return (
    <div className={styles.loaderContainer}>
      <motion.div
        className={styles.loader}
        variants={loaderVariants}
        animate="animation"
      ></motion.div>
      <h2>Loading...</h2>
      {wait && <p>a refresh might help...</p>}
    </div>
  );
};

export default IsLoading;
