### Hey, new shoes?

This is just a fan page for Brian David Gilbert to display gifs from his video content.

This site runs on React and is entirely single page with no routes even. It also uses framer motion for animations and iconify for most of the icons.

Gifs are pulled from their respective hosts on either Gfycat or Tenor based on what is listed in the gif-list.json.

I hope you enjoy it!
